var app = angular.module( 'FitnessPresserApp', [] );

app.controller( 'FitnessPresserCtrl', function( $scope, $http ) {

	$scope.workout = [];
	$scope.exercise = '';

	$scope.exercises = [
		{
			id: 1,
			name: 'Bench Press'
		},
		{
			id: 2,
			name: 'Incline Flyes'
		},
		{
			id: 3,
			name: 'Cable Cross'
		},
		{
			id: 4,
			name: 'Tricep Extension'
		},
		{
			id: 5,
			name: 'Skull Crushers'
		}
	];

	$scope.addSet = function( index ) {
		$scope.workout[ index ].sets.push({ 'weight': '', 'reps': '' });

	}

	$scope.removeSet = function( exerciseIndex, index ) {
		$scope.workout[ exerciseIndex ].sets.splice( index, 1 );

	}

	$scope.addExercise = function() {
		var exercise = $scope.exercise;
		exercise.sets = [{ weight: '', reps: '' }];
		$scope.workout.push( exercise );

	}

	$scope.save = function() {
		$http.post( 'submit.php', $scope.workout );

	}


});
